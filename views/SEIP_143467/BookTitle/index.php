<?php
require_once ("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;


$objBookTitle=new BookTitle();

$allData=$objBookTitle->index("OBJ");
?>


<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 800px;
            height: 800px;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #269abc;
        }
    </style>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<table>
    <tr>
        <th>Id</th>
        <th>Book Title</th>
        <th>Author name</th>
        <th>Actions</th>
    </tr>
    <tr>
        <?php foreach ($allData as $oneData){ ?>

        <td><?php echo $oneData->id."<br>" ?></td>
        <td><?php echo $oneData->book_title."<br>" ?></td>
        <td><?php echo $oneData->author_name."<br>"  ?></td>

        <td class="link">
            <a id ="a" href="view.php?id=<?php echo $oneData->id ?>" class="btn btn-primary" role="button">View</a>
            <a id ="a" href="edit.php?id=<?php echo $oneData->id ?>" class="btn btn-info" role="button">Edit</a>
            <a id ="a" href="delete.php?id=<?php echo $oneData->id ?>" class="btn btn-danger" role="button">Delete</a>
            <a id ="a" href="trash.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button">Trash</a>
        </td>
        </tr>
  <?php }?>
</table>


</body>
</html>