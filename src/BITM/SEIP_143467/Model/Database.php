<?php

namespace App\Model;
use PDO;
use PDOException;
class Database
{

 public $DBH;
    public $host = "localhost";
    public $dbname = "atomic_project_princy";
    public $user = "root";
    public $pass = "";

    public function __construct()
    {
        try {

            $host = $this->host;
            $dbname = $this->dbname;
            $user = $this->user;
            $pass = $this->pass;


            $this->DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
            echo "Connected sucessfully.<br>";
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



        }
        catch (PDOException $e) {
            echo "I'm sorry,not connected.";
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
        }
    }
}
